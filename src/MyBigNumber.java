public class MyBigNumber {
    public static String sum(String stn1, String stn2) {
        int stn1length = stn1.length();                                          // Length of st1
        int stn2length = stn2.length();                                          // Length of st2 
        int maxlength = Math.max(stn1length, stn2length);                        // returns the maximum length
        int num1, num2, sum, memNum = 0;                                        // num1 and num2 save the value of corresponding digits; sum saves the sum of two digits and memNumber saves the memory value.
        StringBuilder result = new StringBuilder("");                      
        for (int i = 0; i < maxlength; i++) {
            if (i < stn1length) {
                num1 = stn1.charAt(stn1length - 1 - i) - '0';                   // Get the digit from the first string, if at the end of the string get the value 0
            } else {
                num1 = 0;
            }
            if (i < stn2length) {
                num2 = stn2.charAt(stn2length - 1 - i) - '0';                   // Get the digit from the second string, if at the end of the string get the value 0
            } else {
                num2 = 0;
            }
            sum = num1 + num2 + memNum;
            if (sum >= 10) {
                memNum = 1;
                sum -=10;
            } else {
                memNum = 0;
            }
            result.append(sum);
        }
        if (memNum > 0) {
            result.append(memNum);
        }
        return result.reverse().toString();                                 //Reverse the result(Because of adding from right to left)
    }
}
